%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka

-define(APP, pubsuber).
-define(TIMEOUT, 50).

-record(pubsuber_channel, {
          name :: term(),
          name_node :: {term(), atom()},
          pid :: pid()
         }).

-record(pubsuber_id, {
          name :: term(),
          id = 1 :: integer()
         }).

%% TODO: Replace with a proper logging framework (like lager).
-define(TS,
        begin
            {_, _, Ms} = Now = now(),
            {{YY, MM, DD}, {H, M, S}} = calendar:now_to_local_time(Now),
            io_lib:format("~4..0w.~2..0w.~2..0w-~2..0w:~2..0w:~2..0w.~p",
                          [YY, MM, DD, H, M, S, Ms])
        end).

-define(DEBUG(Message), io:format("~s: ~s~n", [?TS, Message])).
-define(DEBUG(Format, Args),
        begin
            Str = io_lib:format(Format, Args),
            io:format("~s: ~s~n", [?TS, Str])
        end).
