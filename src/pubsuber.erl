%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Application API module.
-module(pubsuber).

%% API
-export([start/0,
         stop/0]).

-include("pubsuber.hrl").

%%------------------------------------------------------------------------------
%% API functions
%%------------------------------------------------------------------------------

start() ->
    application:start(?APP).

stop() ->
    application:stop(?APP).
