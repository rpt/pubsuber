%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc PubSub channel.
-module(pubsuber_channel).

-behaviour(gen_server).

%% API
-export([subscribe/2,
         unsubscribe/2,
         publish/2]).

%% Internal API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-include("pubsuber.hrl").

-record(state, {
          name :: term(),
          subscribers = sets:new() :: set(),
          next_id :: integer(),
          backlog = [] :: [{Id :: integer(), Message :: bitstring()}]
         }).

%%------------------------------------------------------------------------------
%% API functions
%%------------------------------------------------------------------------------

%% @doc Subscribe to a channel.
-spec subscribe(pid(), term()) -> ok.
subscribe(Pid, Channel) ->
    CPid = case pubsuber_mnesia:read_local(Channel) of
               [ChannelPid] ->
                   %% Happy case
                   ChannelPid;
               [] ->
                   case create(Channel) of
                       {ok, ChannelPid} ->
                           ?DEBUG("Channel ~p created", [Channel]),
                           ChannelPid;
                       {error, {already_exists, ChannelPid}} ->
                           ChannelPid
                   end
           end,
    gen_server:cast(CPid, {subscribe, Pid}).

%% @doc Unsubscribe from a channel.
-spec unsubscribe(pid(), atom()) -> ok.
unsubscribe(Pid, Channel) ->
    case pubsuber_mnesia:read_local(Channel) of
        [ChannelPid] ->
            gen_server:cast(ChannelPid, {unsubscribe, Pid});
        [] ->
            ok
    end.

%% @doc Publish a message to a channel.
-spec publish(atom(), bitstring()) -> ok.
publish(Channel, Message) ->
    case pubsuber_mnesia:read(Channel) of
        [] ->
            ?DEBUG("Publishing to channel without subscribers"),
            ok;
        Channels ->
            Id = pubsuber_mnesia:update_id(Channel),
            [send_message(C, Message, Id) || C <- Channels],
            ok
    end.

%%------------------------------------------------------------------------------
%% Internal API functions
%%------------------------------------------------------------------------------

%% @doc Start a named channel.
-spec start_link(term()) -> {ok, Pid :: pid()} | ignore |
                            {error, Error :: term()}.
start_link(Name) ->
    gen_server:start_link(?MODULE, Name, []).

%%------------------------------------------------------------------------------
%% gen_server callbacks
%%------------------------------------------------------------------------------

init(Name) ->
    Pid = self(),
    case pubsuber_mnesia:add(Name, Pid) of
        {ok, Pid} ->
            {ok, #state{name = Name}};
        {error, Reason} ->
            {stop, Reason}
    end.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast({subscribe, Pid}, #state{subscribers = Subscribers} = State) ->
    %% Add new subscriber
    {noreply, State#state{subscribers = sets:add_element(Pid, Subscribers)}};
handle_cast({unsubscribe, Pid}, #state{name = Name,
                                       subscribers = Subscribers} = State) ->
    NewSubscribers = sets:del_element(Pid, Subscribers),
    case sets:size(NewSubscribers) of
        0 ->
            %% Destroy the channel after everyone unsubscribed
            ?DEBUG("Channel ~p terminated due to no more subscribers", [Name]),
            {stop, normal, State#state{subscribers = NewSubscribers}};
        _More ->
            %% Remove a subscriber
            {noreply, State#state{subscribers = NewSubscribers}}
    end;
handle_cast({message, Message, FirstId}, #state{next_id = undefined} = State) ->
    %% Publish a message to all subscribers
    do_send_message(Message, State),

    {noreply, State#state{next_id = FirstId + 1}};
handle_cast({message, Message, NextId}, #state{next_id = NextId} = State) ->
    %% Publish a message to all subscribers
    do_send_message(Message, State),

    {noreply, empty_backlog(State)};
handle_cast({message, Message, FutureId},
            #state{next_id = NextId,
                   backlog = Backlog} = State) when FutureId > NextId ->
    %% Add future message to the backlog
    NewBacklog = [{FutureId, Message} | Backlog],

    %% Setup a timeout after which missing message will be skipped
    erlang:send_after(?TIMEOUT, self(), {timeout, FutureId - 1}),

    {noreply, State#state{backlog = NewBacklog}};
handle_cast({message, Message, _}, State) ->
    %% Messages with id smaller than the next awaited one
    ?DEBUG("Lost message: \"~s\"", [Message]),
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({timeout, Id},
            #state{next_id = NextId} = State) when Id >= NextId ->
    NewState = empty_backlog(State#state{next_id = Id}),
    case NewState#state.backlog of
        [] ->
            ok;
        Backlog ->
            %% Setup a timeout after which missing message will be skipped
            HighestMissingId = find_smallest_id(Backlog) - 1,
            erlang:send_after(?TIMEOUT, self(), {timeout, HighestMissingId})
    end,
    {noreply, NewState};
handle_info(_Info, State) ->
    {noreply, State}.

terminate({already_exists, _Pid}, _State) ->
    ok;
terminate(_Reason, #state{name = Name}) ->
    pubsuber_mnesia:remove(Name, self()).

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%------------------------------------------------------------------------------
%% Internal functions
%%------------------------------------------------------------------------------

%% @doc Create new channel.
-spec create(term()) -> {ok, Pid :: pid()} |
                        {ok, Pid :: pid(), Info :: term()} |
                        {error, Error :: term()}.
create(Name) ->
    pubsuber_channel_sup:open_channel(Name).

%% @doc Send a message to a channel.
-spec send_message(pid(), bitstring(), integer()) -> ok.
send_message(Pid, Message, Id) ->
    gen_server:cast(Pid, {message, Message, Id}).

%% @doc Publish a message to all subscribers.
-spec do_send_message(bitstring(), #state{}) -> any().
do_send_message(Message, #state{name = Channel, subscribers = Subscribers}) ->
    [pubsuber_user:send_message(Sub, Channel, Message)
     || Sub <- sets:to_list(Subscribers)].

%% @doc Empty backlog of message.
-spec empty_backlog(#state{}) -> #state{}.
empty_backlog(#state{next_id = NextId, backlog = Backlog} = State) ->
    do_empty_backlog(State#state{next_id = NextId + 1,
                                 backlog = lists:reverse(Backlog)}).

-spec do_empty_backlog(#state{}) -> #state{}.
do_empty_backlog(#state{next_id = NextId, backlog = Backlog} = State) ->
    case lists:keytake(NextId, 1, Backlog) of
        {value, {NextId, Message}, NewBacklog} ->
            do_send_message(Message, State),
            do_empty_backlog(State#state{next_id = NextId + 1,
                                         backlog = NewBacklog});
        false ->
            State#state{backlog = lists:reverse(Backlog)}
    end.

-spec find_smallest_id([{integer(), bitstring()}]) -> integer().
find_smallest_id(Backlog) ->
    lists:min([Id || {Id, _Message} <- Backlog]).
