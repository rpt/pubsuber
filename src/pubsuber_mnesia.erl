%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Module for handling Mnesia related operations.
-module(pubsuber_mnesia).

%% API
-export([setup/0,
         add/2,
         read/1,
         read_local/1,
         update_id/1,
         remove/2]).

-include("pubsuber.hrl").

%%------------------------------------------------------------------------------
%% API functions
%%------------------------------------------------------------------------------

%% @doc Setup Mnesia.
-spec setup() -> any().
setup() ->
    %% Make sure there's a schema on disc
    mnesia:change_table_copy_type(schema, node(), disc_copies),

    %% Make sure the tables are properly configured
    ChannelTabDef = [{attributes, record_info(fields, pubsuber_channel)},
                     {ram_copies, [node()]},
                     {type, bag},
                     {index, [#pubsuber_channel.name_node]}],
    mnesia:create_table(pubsuber_channel, ChannelTabDef),

    IdTabDef = [{attributes, record_info(fields, pubsuber_id)},
                {ram_copies, [node()]}],
    mnesia:create_table(pubsuber_id, IdTabDef),

    mnesia:wait_for_tables([pubsuber_channel, pubsuber_id], 5000),

    mnesia:add_table_copy(pubsuber_channel, node(), ram_copies),
    mnesia:add_table_copy(pubsuber_id, node(), ram_copies).

%% @doc Add new instance of the channel.
-spec add(term(), pid()) -> {ok, Pid :: pid()} | {error, {already_exists, Pid :: pid()}}.
add(Channel, Pid) ->
    F = fun() ->
                Node = node(),
                case mnesia:index_read(pubsuber_channel, {Channel, Node},
                                       #pubsuber_channel.name_node) of
                    [] ->
                        New = #pubsuber_channel{name = Channel,
                                                name_node = {Channel, Node},
                                                pid = Pid},
                        mnesia:write(New),
                        Pid;
                    [#pubsuber_channel{pid = ChannelPid}] ->
                        mnesia:abort({already_exists, ChannelPid})
                end
        end,
    case mnesia:transaction(F) of
        {atomic, NewPid} ->
            {ok, NewPid};
        {aborted, Reason} ->
            {error, Reason}
    end.

%% @doc Get the list of instances.
-spec read(term()) -> Instances :: [pid()].
read(Channel) ->
    Channels = mnesia:dirty_read({pubsuber_channel, Channel}),
    [Pid || #pubsuber_channel{pid = Pid} <- Channels].

%% @doc Get a pid of local channel process.
-spec read_local(term()) -> [pid()].
read_local(Channel) ->
    Result = mnesia:dirty_index_read(pubsuber_channel, {Channel, node()},
                                     #pubsuber_channel.name_node),
    [Pid || #pubsuber_channel{pid = Pid} <- Result].

%% @doc Update the next message id counter for a channel.
%% The new counter value is returned.
-spec update_id(term()) -> integer().
update_id(Channel) ->
    F = fun() ->
                case mnesia:read({pubsuber_id, Channel}) of
                    [Old] ->
                        NewId = Old#pubsuber_id.id + 1,
                        New = Old#pubsuber_id{id = NewId},
                        mnesia:write(New),
                        NewId;
                    [] ->
                        New = #pubsuber_id{name = Channel,
                                           id = 0},
                        mnesia:write(New),
                        0
                end
        end,
    {atomic, Id} = mnesia:transaction(F),
    Id.

%% @doc Remove an instance of the channel.
-spec remove(term(), pid()) -> any().
remove(Channel, Pid) ->
    F = fun() ->
                Object = #pubsuber_channel{name = Channel,
                                           name_node = {Channel, node()},
                                           pid = Pid},
                mnesia:delete_object(Object)
        end,
    mnesia:transaction(F).
