%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Application module.
-module(pubsuber_app).

-behaviour(application).

%% Application callbacks
-export([start/2,
         stop/1]).

-include("pubsuber.hrl").

%%------------------------------------------------------------------------------
%% Application callbacks
%%------------------------------------------------------------------------------

start(_StartType, _StartArgs) ->
    ?DEBUG("PubSuber starting..."),

    %% Setup Mnesia for the distributed channel registry
    pubsuber_mnesia:setup(),

    %% Start the application
    pubsuber_sup:start_link().

stop(_State) ->
    ?DEBUG("PubSuber stopped"),
    ok.
