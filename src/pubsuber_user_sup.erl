%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Channel supervisor.
-module(pubsuber_user_sup).

-behaviour(supervisor).

%% Internal API
-export([start_link/0,
         open_session/1]).

%% Supervisor callbacks
-export([init/1]).

%%------------------------------------------------------------------------------
%% Internal API functions
%%------------------------------------------------------------------------------

%% @doc Start the channel supervisor.
-spec start_link() -> term().
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% @doc Create a session for new user.
-spec open_session(bitstring()) -> {ok, Pid :: pid()} |
                                   {error, Error :: term()}.
open_session(Name) ->
    supervisor:start_child(?MODULE, [Name]).

%%------------------------------------------------------------------------------
%% Supervisor callbacks
%%------------------------------------------------------------------------------

init([]) ->
    UserSpec = {pubsuber_user, {pubsuber_user, start_link, []},
                temporary, 1000, worker, [pubsuber_user]},
    {ok, {{simple_one_for_one, 5, 10}, [UserSpec]}}.
