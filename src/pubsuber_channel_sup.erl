%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Channel supervisor.
-module(pubsuber_channel_sup).

-behaviour(supervisor).

%% Internal API
-export([start_link/0,
         open_channel/1]).

%% Supervisor callbacks
-export([init/1]).

%%------------------------------------------------------------------------------
%% Internal API functions
%%------------------------------------------------------------------------------

%% @doc Start the channel supervisor.
-spec start_link() -> term().
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% @doc Open new named channel.
-spec open_channel(term()) -> {ok, Pid :: pid()} |
                              {ok, Pid :: pid(), Info :: term()} |
                              {error, Error :: term()}.
open_channel(Name) ->
    supervisor:start_child(?MODULE, [Name]).

%%------------------------------------------------------------------------------
%% Supervisor callbacks
%%------------------------------------------------------------------------------

init([]) ->
    ChannelSpec = {pubsuber_channel, {pubsuber_channel, start_link, []},
                   temporary, 1000, worker, [pubsuber_channel]},
    {ok, {{simple_one_for_one, 5, 10}, [ChannelSpec]}}.
