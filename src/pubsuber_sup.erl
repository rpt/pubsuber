%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc Main application supervisor.
-module(pubsuber_sup).

-behaviour(supervisor).

%% Internal API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%%------------------------------------------------------------------------------
%% Internal API functions
%%------------------------------------------------------------------------------

%% @doc Start the channel supervisor.
-spec start_link() -> term().
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%------------------------------------------------------------------------------
%% Supervisor callbacks
%%------------------------------------------------------------------------------

init([]) ->
    ChannelSup = {pubsuber_channel_sup, {pubsuber_channel_sup, start_link, []},
                  permanent, 1000, supervisor, [pubsuber_channel_sup]},
    UserSup = {pubsuber_user_sup, {pubsuber_user_sup, start_link, []},
               permanent, 1000, supervisor, [pubsuber_user_sup]},
    {ok, {{one_for_all, 5, 10}, [ChannelSup, UserSup]}}.
