%% @author Krzysztof Rutka <krzysztof.rutka@gmail.com>
%% @copyright 2012 Krzysztof Rutka
%% @doc PubSub client.
-module(pubsuber_user).

-behaviour(gen_server).

%% API
-export([connect/1]).

%% Internal API
-export([start_link/1,
         send_message/3]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-include("pubsuber.hrl").

-record(state, {
          name :: bitstring()
         }).

%%------------------------------------------------------------------------------
%% API functions
%%------------------------------------------------------------------------------

%% @doc A new user connects to the server.
-spec connect(bitstring()) -> {ok, Pid :: pid()} |
                              {error, Error :: term()}.
connect(Name) ->
    pubsuber_user_sup:open_session(Name).

%%------------------------------------------------------------------------------
%% Internal API functions
%%------------------------------------------------------------------------------

%% @doc Start a new user session.
-spec start_link(bitstring()) -> {ok, Pid :: pid()} | ignore |
                                 {error, Error :: term()}.
start_link(Name) ->
    gen_server:start_link(?MODULE, Name, []).

%% @doc Send a message to a user subscribed to a channel.
-spec send_message(pid(), term(), bitstring()) -> ok.
send_message(Pid, Channel, Message) ->
    gen_server:cast(Pid, {message, Channel, Message}).

%%------------------------------------------------------------------------------
%% gen_server callbacks
%%------------------------------------------------------------------------------

init(Name) ->
    ?DEBUG("User ~s connected", [Name]),
    {ok, #state{name = Name}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast({message, Channel, Message}, #state{name = Name} = State) ->
    ?DEBUG("~s received \"~s\" on channel ~p", [Name, Message, Channel]),
    {noreply, State};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
