.PHONY: compile console first second dialyzer test clean

compile:
	@./rebar compile

console: compile
	@erl -pa ebin -sname pubsuber -s mnesia -s pubsuber

first: compile
	@erl -pa ebin -sname first@localhost \
	    -mnesia extra_db_nodes "[second@localhost]" -s mnesia -s pubsuber

second: compile
	@erl -pa ebin -sname second@localhost \
	    -mnesia extra_db_nodes "[first@localhost]" -s mnesia -s pubsuber

dialyzer: build.plt compile
	dialyzer --plt $< ebin

build.plt:
	dialyzer -q --build_plt --apps kernel stdlib mnesia --output_plt $@

test:
	@./rebar skip_deps=true eunit

clean:
	@./rebar clean
