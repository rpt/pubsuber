# PubSuber

Distributed PubSub server prototype written in Erlang.

Tested with Erlang R15B02 on MacOS 10.8.2.

## How to use it

 * Use `make` to build it,
 * Use `make first` and `make second` to start two nodes with preconfigured Mnesia.

## API

    pubsuber_user:connect(UserName :: bitstring()) -> {ok, Pid :: pid()}.
    pubsuber_channel:subscribe(UserPid :: pid(), ChannelName :: term()) -> ok.
    pubsuber_channel:publish(ChannelName :: term(), Message :: bitstring()) -> ok.
    pubsuber_channel:unsubscribe(UserPid :: pid(), ChannelName :: term()) -> ok.
